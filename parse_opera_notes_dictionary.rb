#!/usr/bin/env ruby
require 'fileutils'

f = File.readlines("/home/ardath/.opera/notes.adr")

f.each do |line|
  next unless line.match("»")
  line.gsub!(/.*NAME=/, "")
  # puts line

  a, b = line.split("»").each do |e|
     e.gsub!("\u0002", "")
     e.gsub!(/\b/, "")
     e.gsub!(/\t/, "")
     e.gsub!(/\s$/, "")
     e.gsub!(/^\s*/, "")
     e.chomp!
  end

  b = b.split("\n")

  a.gsub!("/", "_")
  a.gsub!("-", "_")
  a.chomp!
  # puts "a:#{a}."

  if (a.length > 100)
    a = a[0..30]
  end

  dir = "slowka"
  FileUtils.mkdir_p dir
  if (!a.empty?)
    file = "#{dir}/#{a}.txt"
    unless File.exist? file
      puts a
      puts b
      File.open(file, "w") do |f|
        f.print b.join("\n")
      end
    end
  end
end
